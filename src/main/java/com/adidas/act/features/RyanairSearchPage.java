package com.adidas.act.features;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.concurrent.TimeUnit;


/**
 * Created by Pablo de la Vega on 23/03/18
 */

@DefaultUrl("https://www.ryanair.com/es/es/")

public class RyanairSearchPage extends PageObject {

    @FindBy(css = "input[placeHolder='Aeropuerto de salida']")
    public WebElement originInputField;

    @FindBy(css = "input[placeHolder='Aeropuerto de destino']")
    public WebElement destinationInputField;

    public RyanairSearchPage(WebDriver driver) {
        super(driver);
    }

    public void closeCookies(){
        WebElement cookie = getDriver().findElement(By.cssSelector("core-icon[ng-click='acceptCookiePolicy();']"));
        withTimeoutOf(10,TimeUnit.SECONDS).waitFor(cookie).click();
    }

    public void searchForOrigin(String searchRequest) {
        closeCookies(); //This function is used to close the cookies bar

        element(originInputField).clear();
        element(originInputField).typeAndEnter(searchRequest);
    }

    public void searchForDestination(String searchRequest) {
        element(destinationInputField).clear();
        element(destinationInputField).typeAndEnter(searchRequest);

        WebElement dayOrigin = getDriver().findElement(By.cssSelector("li[data-id='30-04-2018']"));
        withTimeoutOf(10, TimeUnit.SECONDS).waitFor(dayOrigin).click();

        WebElement dayDestination = getDriver().findElement(By.cssSelector("li[data-id='15-05-2018']"));
        withTimeoutOf(10, TimeUnit.SECONDS).waitFor(dayDestination).click();

        WebElement dropdown = getDriver().findElement(By.cssSelector("div[ng-click='toggleDropdown($event)']"));
        withTimeoutOf(10, TimeUnit.SECONDS).waitFor(dropdown).click();

        WebElement adults = getDriver().findElement(By.cssSelector("div[label='Adultos']"));
        WebElement incrementAdults =  adults.findElement(By.cssSelector("button[ng-click='$ctrl.increment()']"));
        withTimeoutOf(10, TimeUnit.SECONDS).waitFor(incrementAdults).click();

        WebElement teens = getDriver().findElement(By.cssSelector("div[label='Adolescentes']"));
        WebElement incrementTeens =  teens.findElement(By.cssSelector("button[ng-click='$ctrl.increment()']"));
        withTimeoutOf(10, TimeUnit.SECONDS).waitFor(incrementTeens).click();

    }

    public void searchOriginDestination(String searchRequestOrigin, String searchRequestDestination) {
        closeCookies(); //This function is used to close the cookies bar

        element(originInputField).clear();
        element(originInputField).type(searchRequestOrigin);

        element(destinationInputField).clear();
        element(destinationInputField).typeAndEnter(searchRequestDestination);

        WebElement dayOrigin = getDriver().findElement(By.cssSelector("li[data-id='30-04-2018']"));
        withTimeoutOf(10, TimeUnit.SECONDS).waitFor(dayOrigin).click();

        WebElement dayDestination = getDriver().findElement(By.cssSelector("li[data-id='15-05-2018']"));
        withTimeoutOf(10, TimeUnit.SECONDS).waitFor(dayDestination).click();

        WebElement dropdown = getDriver().findElement(By.cssSelector("div[ng-click='toggleDropdown($event)']"));
        withTimeoutOf(10, TimeUnit.SECONDS).waitFor(dropdown).click();

        WebElement adults = getDriver().findElement(By.cssSelector("div[label='Adultos']"));
        WebElement incrementAdults =  adults.findElement(By.cssSelector("button[ng-click='$ctrl.increment()']"));
        withTimeoutOf(10, TimeUnit.SECONDS).waitFor(incrementAdults).click();

        WebElement teens = getDriver().findElement(By.cssSelector("div[label='Adolescentes']"));
        WebElement incrementTeens =  teens.findElement(By.cssSelector("button[ng-click='$ctrl.increment()']"));
        withTimeoutOf(10, TimeUnit.SECONDS).waitFor(incrementTeens).click();

    }

    public RyanairResultsPage pushButton(){
        WebElement pushButton = getDriver().findElement(By.cssSelector("button[ng-keypress='searchFlights()']"));
        withTimeoutOf(10,TimeUnit.SECONDS).waitFor(pushButton).click();
        return new RyanairResultsPage(getDriver());
    }

}
