@ready
Feature: Open Ryanair web

  @test
  Scenario: Ryanair web
    Given I want to open Ryanair web
    When I search for origin 'Madrid'
    And I search for destination 'Londres Stansted'
    Then I click the search button
    And I should see 'Madrid'

  @notest
  Scenario Outline:  Ryanair web multiple
    Given I want to open Ryanair web
    When  I search for origin '<search_origin>'
    And I search for destination '<search_destination>'
    Then I click the search button
  Examples:
      | search_origin     | search_destination |
      | Barcelona-El Prat | Londres Stansted   |
      | Madrid            | Londres Stansted   |

  @notest
  Scenario Outline: Multiple search
    Given I want to open Ryanair web
    When I want to go from '<origin_travel>' , to  '<destination_travel>'
    Then I click the search button
  Examples:
      | origin_travel     | destination_travel | date_origin_travel | date_destination_travel |
      | Barcelona-El Prat | Londres Stansted   | 25-04-2018         | 15-05-2018              |
      | Madrid            | Londres Stansted   | 25-04-2018         | 15-05-2018              |