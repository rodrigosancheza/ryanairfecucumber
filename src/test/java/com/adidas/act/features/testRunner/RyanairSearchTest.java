package com.adidas.act.features.testRunner;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

/**
 * Created by Pablo de la Vega on 23/03/18
 */

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = { "src/test/resources/features" },
        plugin = {"pretty", "html:target/cucumber", "json:target/cucumber-report.json"},
        glue = { "com.adidas.act.features.stepDefinitions" }
)

public class RyanairSearchTest {
}
