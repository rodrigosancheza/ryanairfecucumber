package com.adidas.act.features.serenitySteps;

import com.adidas.act.features.RyanairResultsPage;
import com.adidas.act.features.RyanairSearchPage;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

import java.util.List;


public class RyanairSearchSteps {

    RyanairSearchPage searchPage;
    RyanairResultsPage resultsPage;

    @Step
    public void openRyanairSearchPage() {
        searchPage.open();
    }

    @Step
    public void searchForOrigin(String searchRequest){
        searchPage.searchForOrigin(searchRequest);
    }

    @Step
    public void searchForDestination(String searchRequest){
        searchPage.searchForDestination(searchRequest);
    }

    @Step
    public void searchOriginDestination(String searchRequestOrigin, String searchRequestDestination){
        searchPage.searchOriginDestination(searchRequestOrigin, searchRequestDestination);
    }

    @Step
    public void pushButton(){
        searchPage.pushButton();
    }

    @Step
    public void verifyResult(String searchResult) {
        List<String> results = resultsPage.getResultsList();
        Assert.assertTrue(results.contains(searchResult));
    }

}
