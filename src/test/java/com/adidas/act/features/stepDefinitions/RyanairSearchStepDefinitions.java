package com.adidas.act.features.stepDefinitions;

import com.adidas.act.features.serenitySteps.RyanairSearchSteps;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class RyanairSearchStepDefinitions {

    @Steps
    RyanairSearchSteps ryanairSearchSteps;

    @Given("I want to open Ryanair web")
    public void iWantToSearchInGoogle() throws Throwable {
        ryanairSearchSteps.openRyanairSearchPage();
    }

    @When("I search for origin '(.*)'")
    public void iSearchForOrigin(String searchRequest) throws Throwable{
        ryanairSearchSteps.searchForOrigin(searchRequest);
    }

    @And("I search for destination '(.*)'")
    public void iSearchForDestination(String searchRequest) throws Throwable{
        ryanairSearchSteps.searchForDestination(searchRequest);
    }

    @When ("I want to go from '(.*)' , to  '(.*)'")
    public void iWantToGoFrom(String searchRequestOrigin, String searchRequestDestination) throws Throwable{
        ryanairSearchSteps.searchOriginDestination(searchRequestOrigin, searchRequestDestination);
    }

    @Then ("I click the search button")
    public void iClickTheSearchButton() throws Throwable{
        ryanairSearchSteps.pushButton();
    }

    @And("I should see '(.*)'")
    public void iShouldSee(String searchResult) throws Throwable {
        ryanairSearchSteps.verifyResult(searchResult);
    }

}
